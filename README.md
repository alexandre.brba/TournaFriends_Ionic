# R�sum�

TournaFriends est une application qui permet l'**organisation de tournois pour des particuliers**. On peut consulter la liste des tournois et des participants. A la cr�ation du tournoi l�utilisateur indique la date, le jeu, le lieu... Les autres utilisateurs peuvent s'inscrire � ce tournoi. A l'inscription, une notification est programm�e pour se lancer une heure avant le tournoi pour pr�venir l�utilisateur.

# Documents, outils et plugin

Utilisation d�une **API personnelle** d�ploy�e sur le Google Cloud et qui utilise la base de donn�es datastore de google.
Utilisation du **plugin ionic local notification**.
Le projet poss�de un dossier **screenshots** afin de montrer les r�sultats de l'application.
Le dossier **documents** contient le document **"Explications des screenshots.pdf"** expliquant ces screenshots. Celui contient �galement un dossier contenant les sources JSON de l'API REST (avec les deux entit�s Tournament et Participant).

# Fonctionnalit�s

* Liste des tournois

La premi�re page renvoie directement sur la page montrant la liste des tournois. Les anciens tournois sont masqu�s lorsque la date est d�pass�e.
Pour chaque tournoi, il y a : un titre, une url menant vers une image png, le nom d�un jeu, la date (jours, mois, ann�es, heures et minutes), le prix d�entr�e pour le tournoi et le nombre de participants qui s�incr�mente dynamiquement.
Puis deux boutons pour chaque tournoi afin d�avoir la liste des participants et l�autre, d�y participer.

* Liste des participants

Le nom et le mail de chaque participant du tournoi concern�.

* Participer � un tournoi

Il est �galement possible de participer au tournoi en remplissant le formulaire indiquant le nom et le mail de l'utilisateur.
Lorsque l�utilisateur clique sur � JE PARTICIPE ! �, une notification locale est programm�e pour se lancer une heure avant la date du tournoi renseign�e. La notification apparait avec une led de couleur rouge choisi par mes soins.

* Cr�ation d'un tournoi

En remplissant le formulaire, il est possible de cr�er un tournoi. Les champs Titre, Nom du jeu, Lieu, Jour et Heure sont obligatoires. Le formulaire ne pourra pas �tre valid� si les champs obligatoires ne sont pas remplis. Les champs non obligatoires : url de l'image et prix de l'entr�e.

* Le menu

Le menu pr�sent en bas de l'application permet de naviguer facilement entre les diff�rentes pages.

* Afficher la page de contact

La page de contact indique le mail de contact mais �galement le lien menant vers la page Twitch de l�application. Ce dernier est une plateforme de diffusion sur internet qui aura pour but de diffuser les plus grands tournois avec de gros enjeux.

# Compiler l'apk du projet

* installer nodejs et npm
* cloner le repository ou t�l�charger le zip puis l'extraire
* ouvrir la console et se positionner dans le dossier du projet
* npm install ionic cordova -g
* npm install ionic@4.6.0 -g
* ionic cordova platform add android
* ionic cordova build android (il est n�cessaire d'avoir le sdk en tant que variable d'environnement)

A la fin de l'op�ration, la console vous indique dans quel dossier l'apk a �t� d�pos�e.