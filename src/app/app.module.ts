import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { ContactPage } from '../pages/contact/contact';
import { TournamentPage } from '../pages/tournament/tournament';
import { jeParticipePage } from '../pages/jeParticipe/jeParticipe';
import { createTournamentPage } from '../pages/createTournament/createTournament';
import { ParticipantPage } from '../pages/participant/participant';
import { TabsPage } from '../pages/tabs/tabs';

import { HttpClientModule } from '@angular/common/http';
import { TournamentsProvider } from '../providers/tournaments/tournaments';
import { LocalNotifications } from '@ionic-native/local-notifications';

@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    TournamentPage,
    jeParticipePage,
    createTournamentPage,
    ParticipantPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactPage,
    TournamentPage,
    jeParticipePage,
    createTournamentPage,
    ParticipantPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TournamentsProvider,
    LocalNotifications
  ]
})
export class AppModule {}
