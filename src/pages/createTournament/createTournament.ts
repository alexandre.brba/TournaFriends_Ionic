import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TournamentsProvider } from '../../providers/tournaments/tournaments';

@Component({
  selector: 'page-createTournament',
  templateUrl: 'createTournament.html'
})
export class createTournamentPage{
  data = { nom:'', url:'', jeu:'', lieu:'', prix:'', date:'', time:'' };
  constructor(public navCtrl: NavController, private tournamentsProvider : TournamentsProvider) {
  }

  //Fonction de submit du formulaire qui permet de creer un tournoi
  submit(){
    this.tournamentsProvider.createTournament(this.data);
    this.data = { nom:'', url:'', jeu:'', lieu:'', prix:'', date:'', time:'' };
  }

}
