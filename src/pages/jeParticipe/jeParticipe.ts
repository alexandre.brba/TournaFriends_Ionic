import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { TournamentsProvider } from '../../providers/tournaments/tournaments';

@Component({
  selector: 'page-jeParticipe',
  templateUrl: 'jeParticipe.html'
})
export class jeParticipePage{
  idTournoi; //Identifiant du tournoi
  jeu; //Le nom du jeu du tournoi
  date; //La date du tournoi
  data = { nom:'', email:''}; // Informations du participant

  //Le constructeur recupere tous les parametres necessaires a la creation du participant
  constructor(public navCtrl: NavController, public navParams: NavParams, public localNotifications: LocalNotifications, private tournamentsProvider : TournamentsProvider) {
    this.idTournoi=this.navParams.get('id');
    this.date=this.navParams.get('date');
    this.jeu=this.navParams.get('jeu');
  }

  //Ajoute le participant au tournoi puis programme la notification une heure avant le tournoi
  submit(){
    var date = new Date(this.date);

    date.setHours(date.getHours() - 1);
    let participant = { idTournoi:this.idTournoi ,nom:this.data.nom, email:this.data.email};
    this.tournamentsProvider.createParticipant(participant);
    this.localNotifications.schedule({
        text: "Salut! N'oublie pas le tournoi de "+this.jeu+" dans 1h00 !",
        at: date,
        led: 'FF0000'
    });
    
    this.jeu = "";
    this.date = "";
    this.data = { nom:'', email:''};
  }
}
