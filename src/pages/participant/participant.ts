import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TournamentsProvider } from '../../providers/tournaments/tournaments';

@Component({
  selector: 'page-participant',
  templateUrl: 'participant.html'
})
export class ParticipantPage{
  idTournoi; //Identifiant du tournoi
  participants; //La liste des participants du tournoi
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private tournamentsProvider : TournamentsProvider) {
    this.idTournoi=this.navParams.get('id');
    this.tournamentsProvider.getParticipants(this.idTournoi).then((participants)=>{
      this.participants = participants;
    },  (err)=>{
      console.log(err);
    })
  }
}
