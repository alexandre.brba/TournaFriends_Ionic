import { Component } from '@angular/core';

import { createTournamentPage } from '../createTournament/createTournament';
import { TournamentPage } from '../tournament/tournament';
import { ContactPage } from '../contact/contact';

@Component({
  templateUrl: 'tabs.html'
})

//Cete page permet l'affichage du menu
export class TabsPage {

  tab1Root = TournamentPage;
  tab2Root = createTournamentPage;
  tab3Root = ContactPage;

  constructor() {

  }
}
