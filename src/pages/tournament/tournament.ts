import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TournamentsProvider } from '../../providers/tournaments/tournaments';
import { jeParticipePage } from '../jeParticipe/jeParticipe';
import { ParticipantPage } from '../participant/participant';

@Component({
  selector: 'page-tournament',
  templateUrl: 'tournament.html'
})
export class TournamentPage{
  tournaments; //Les tournois
  today; //La date d'aujourd'hui
  constructor(public navCtrl: NavController, private tournamentsProvider : TournamentsProvider) {
    this.today = Date.now();
    this.tournamentsProvider.getTournament().then((tournaments)=>{
      this.tournaments = tournaments;
    },  (err)=>{
      console.log(err);
    })
  }

  //Permet de s'assurer du rafraichissement des tournois
  ionViewWillEnter(){
    this.tournamentsProvider.getTournament().then((tournaments)=>{
      this.tournaments = tournaments;
    },  (err)=>{
      console.log(err);
    })
  }

  //Renvoie vers la page pour créer un participant
  jeParticipe(id,date,jeu){
    let param = { 'id': id , 'date': date, 'jeu': jeu};
    this.navCtrl.push(jeParticipePage,param)
  }

  //Renvoie vers la page qui affiche la liste des participants
  listParticipants(id){
    let param = { 'id': id };
    this.navCtrl.push(ParticipantPage,param)
  }

}
