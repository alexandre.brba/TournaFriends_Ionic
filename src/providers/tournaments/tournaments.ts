import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TournamentsProvider {

  tournamentUrl = "https://flawless-parity-147007.appspot.com"; //URL de l'API personnelle hebergé sur le Datastore de Google

  constructor(public http: HttpClient) {
  }

//Récupère la liste des tournois
getTournament() {
  return new Promise((resolve, reject) => {
    this.http.get(`${this.tournamentUrl}/tournaments`).subscribe(
      resp => {
        resolve(resp);
      },
      err => {
        reject(err);
      }
    );
  });
}

//Créer un tournoi à partir du tableau data fournie
createTournament(data) {
  return new Promise((resolve, reject) => {
    this.http.put(`${this.tournamentUrl}/createTournament`,data).subscribe(
      resp => {
        resolve(resp);
      },
      err => {
        reject(err);
      }
    );
  });
}

//Récupère la liste des participants à partir d'un identifiant de tournoi
getParticipants(tournoiId){
  return new Promise((resolve, reject) => {
    this.http.get(`${this.tournamentUrl}/participants/`+tournoiId).subscribe(
      resp => {
        resolve(resp);
      },
      err => {
        reject(err);
      }
    );
  });
}

//Créer un participant pour un tournoi donné
createParticipant(data){
  return new Promise((resolve, reject) => {
    this.http.put(`${this.tournamentUrl}/createParticipant`,data).subscribe(
      resp => {
        resolve(resp);
      },
      err => {
        reject(err);
      }
    );
  });
}

}
